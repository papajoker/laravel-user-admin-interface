<?php
/**
 * 
 */

function auth_translation($part = "")
{
	return Lang::get('validate-laravel-auth::validation' . ".{$part}");
}

function validation_translation($part = "")
{
	return Lang::get('validate-laravel::validation' . ".{$part}");
}

?>
