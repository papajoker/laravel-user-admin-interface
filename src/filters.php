<?php

Route::filter('useradmin.auth', function()
{
	if (Auth::guest())
	{
		return Redirect::guest(URL::route('useradmin.auth.login'));
	}
});

?>