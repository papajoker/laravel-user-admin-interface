@extends(base_layout())

{{-- Login page layout --}}

@section('body')
<div class="row">
	<div class="large-12 columns">

		<h2>Change Password</h2>
		{{ Form::open(array('route' => array('useradmin.profile.change-password'))) }}

		<p>You may choose a new password here.</p>

		<!-- Existing Password -->
		<div class="row collapse {{{ $errors->has('password') ? 'error' : '' }}}">
			<div class="large-2 columns">
				<span class="prefix radius">{{ Form::label('password', 'Existing Password') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::password('password') }}
				{{ $errors->first('password', '<small class="error">:message</small>') }}
			</div>
			<div class="large-7 columns">&nbsp;</div>
		</div>

		<!-- New Password -->
		<div class="row collapse {{{ $errors->has('new_password') ? 'error' : '' }}}">
			<div class="large-2 columns">
				<span class="prefix radius">{{ Form::label('new_password', 'New Password') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::password('new_password') }}
				{{ $errors->first('new_password', '<small class="error">:message</small>') }}
			</div>
			<div class="large-7 columns">&nbsp;</div>
		</div>

		<!-- Confirm Password -->
		<div class="row collapse {{{ $errors->has('new_password_confirmation') ? 'error' : '' }}}">
			<div class="large-2 columns">
				<span class="prefix radius">{{ Form::label('new_password_confirmation', 'Confirm Password') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::password('new_password_confirmation') }}
				{{ $errors->first('new_password_confirmation', '<small class="error">:message</small>') }}
			</div>
			<div class="large-7 columns">&nbsp;</div>
		</div>

		<!-- Submit button -->
		<div class="control-group">
			<div class="controls">
				{{ Form::submit('Change Password', array('class' => 'button medium radius')) }}
			</div>
		</div>

		{{ Form::close() }}
	</div>
</div>
@stop
