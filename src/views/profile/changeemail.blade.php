@extends(base_layout())

{{-- Login page layout --}}

@section('body')
<div class="row">
	<div class="large-12 columns">

		<h2>Change Email</h2>
		{{ Form::open(array('route' => array('useradmin.profile.change-email'))) }}

		<p>You may choose a new email address here. Enter your current password for validation.</p>

		<p>Your current email address is {{ current_email() }}</p>

		<!-- New Email -->
		<div class="row collapse {{{ $errors->has('email') ? 'error' : '' }}}">
			<div class="large-1 columns">
				<span class="prefix radius">{{ Form::label('email', 'Email') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::text('email', Input::old('email')) }}
				{{ $errors->first('email', '<small class="error">:message</small>') }}
			</div>
			<div class="large-8 columns">&nbsp;</div>
		</div>

		<!-- Password -->
		<div class="row collapse {{{ $errors->has('password') ? 'error' : '' }}}">
			<div class="large-1 columns">
				<span class="prefix radius">{{ Form::label('password', 'Password') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::password('password') }}
				{{ $errors->first('password', '<small class="error">:message</small>') }}
			</div>
			<div class="large-8 columns">&nbsp;</div>
		</div>

		<!-- Submit button -->
		<div class="control-group">
			<div class="controls">
				{{ Form::submit('Change Email', array('class' => 'button medium radius')) }}
			</div>
		</div>

		{{ Form::close() }}
	</div>
</div>
@stop
