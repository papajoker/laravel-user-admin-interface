<?php
/**
 * Base layout
 */
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<body>
	@section('alerts')
		@include('useradmin::layouts.alerts')
	@show

	@yield('body')
</body>
</html>