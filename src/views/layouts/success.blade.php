@if ($message = Session::get('success'))
<div class="row">
	<div class="large-12 columns">
		<div data-alert class="alert-box success radius">
			@if (is_bool($message))
				@if(Session::has('reason'))
					{{ Lang::get(Session::get('reason')) }}
				@else
					{{ Lang::get('global.success') }}
				@endif
			@else
				{{ $message }}
			@endif
			<a href="#" class="close">&times;</a>
		</div>
	</div>
</div>
@endif