@extends(base_layout())

{{-- Password reset page layout --}}

@section('alerts')
	@if (Session::get('success') === true)
	<div class="row">
		<div class="large-12 columns">
			<div data-alert class="alert-box success radius">
				{{ Lang::get('useradmin::auth.reset.requested') }}
				<a href="#" class="close">&times;</a>
			</div>
		</div>
	</div>
	@endif
	@include('useradmin::layouts.error')
@stop

@section('body')
	<div class="row">
		<div class="large-12 columns">

			<h2>Forgotten Password</h2>
			{{ Form::open(array('route' => 'useradmin.auth.lost-password')) }}

			<p>Use this form to reset your password - you will receive an email with instructions.</p>

			<!-- Email -->
			<div class="row collapse {{{ $errors->has('email') ? 'error' : '' }}}">
				<div class="large-1 columns">
					<span class="prefix radius">{{ Form::label('email', 'Email') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('email', Input::old('email')) }}
					{{ $errors->first('email', '<small class="error">:message</small>') }}
				</div>
				<div class="large-8 columns">&nbsp;</div>
			</div>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Reset Password', array('class' => 'button medium radius')) }}
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
