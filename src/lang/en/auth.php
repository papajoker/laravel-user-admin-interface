<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Auth Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'login' => array(
		'already' => 'You are already logged in',
		'successful' => 'Logged in successfully',
		'failed' => 'Login failed',
		'loggedout' => 'You have been logged out',
	),

	'reset' => array(
		'logoutfirst' => 'You are already logged in - please log out to reset your password',
		'requested' => 'Password reset request successful - please check your email to proceed with reset',
		'success' => 'Password change successful - you may now log in',
		'failed' => 'Password change failed',
	)
);