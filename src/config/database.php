<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Database connection name
	|--------------------------------------------------------------------------
	|
	| The name of the connection we will use to connect to the database
	| containing the auth tables.
	|
	| If your authentication tables are in a different database to your main
	| application data, or even if they are in the same table, but use a
	| different table prefix, you should create a second database connection
	| for app/config/database.php and specify that name here.
	|
	| Default value: 'default'
	|
	*/

	'connection' => 'default',

	/*
	|--------------------------------------------------------------------------
	| Username field for authentication
	|--------------------------------------------------------------------------
	|
	| The name of the field which stores the username
	|
	| Note - if using the provided database migrations, leave this at the
	| default value.
	|
	| Default value: 'username'
	|
	*/

	'username_field' => 'username',

	/*
	|--------------------------------------------------------------------------
	| Email field for authentication
	|--------------------------------------------------------------------------
	|
	| The name of the field which stores the email address
	|
	| Note - if using the provided database migrations, leave this at the
	| default value.
	|
	|
	| Default value: 'email'
	|
	*/

	'email_field' => 'email',

	/*
	|--------------------------------------------------------------------------
	| Password field for authentication
	|--------------------------------------------------------------------------
	|
	| The name of the field which stores the password
	|
	| Note - if using the provided database migrations, leave this at the
	| default value.
	|
	| Default value: 'password'
	|
	*/

	'password_field' => 'password',

);

?>
