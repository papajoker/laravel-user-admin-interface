<?php
/**
 * 
 */

function current_username()
{
	$username_field = username_field();

	return Auth::user()->$username_field;
}

function current_email()
{
	$email_field = email_field();

	return Auth::user()->$email_field;
}


?>
