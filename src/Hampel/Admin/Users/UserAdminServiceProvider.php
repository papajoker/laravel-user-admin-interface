<?php namespace Hampel\Admin\Users;

use Lang, View, Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Factory;
use Illuminate\Validation\DatabasePresenceVerifier;
use Hampel\Admin\Users\Repositories\UserRepositoryInterface;
use Hampel\Admin\Users\Repositories\EloquentUserRepository;
use Hampel\Admin\Users\Validators\UserValidator;

class UserAdminServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('hampel/useradmin', 'useradmin', __DIR__ . '/../../..');

		include __DIR__ . '/../../../routes.php';
		include __DIR__ . '/../../../filters.php';
		include __DIR__ . '/../../../configHelpers.php';
		include __DIR__ . '/../../../authHelpers.php';
		include __DIR__ . '/../../../langHelpers.php';

		Lang::addNamespace('useradmin', __DIR__ . '/../../../lang');
		View::addNamespace('useradmin', __DIR__ . '/../../../views');

		$this->configDefaults();
		$this->bootValidator();
		$this->bindRepositories();
		$this->bootCommands();
	}

	public function configDefaults()
	{
		$connection = Config::get('useradmin::database.connection');

		if ($connection == 'default')
		{
			Config::set('useradmin::database.connection', Config::get('database.default'));
		}
	}

	public function bindRepositories()
	{
		$this->app->singleton('Hampel\Admin\Users\Repositories\UserRepositoryInterface', function($app)
		{
			$user_validator = new UserValidator($app['useradmin.validator']);

			return new EloquentUserRepository($user_validator);
		});
	}

	public function bootCommands()
	{
		$this->app['useradmin.console.config'] = $this->app->share(function($app)
		{
			return new Console\ConfigCommand();
		});

		$this->app['useradmin.console.prepare'] = $this->app->share(function($app)
		{
			return new Console\PrepareCommand();
		});

		$this->app['useradmin.console.migrate'] = $this->app->share(function($app)
		{
			return new Console\MigrateCommand();
		});

		$this->app['useradmin.console.useradd'] = $this->app->share(function($app)
		{
			$users = $app->make('Hampel\Admin\Users\Repositories\UserRepositoryInterface');

			return new Console\UserAddCommand($users);
		});

		$this->app['useradmin.console.userfind'] = $this->app->share(function($app)
		{
			$users = $app->make('Hampel\Admin\Users\Repositories\UserRepositoryInterface');

			return new Console\UserFindCommand($users);
		});

		$this->app['useradmin.console.userupdate'] = $this->app->share(function($app)
		{
			$users = $app->make('Hampel\Admin\Users\Repositories\UserRepositoryInterface');

			return new Console\UserUpdateCommand($users);
		});

		$this->commands(
			'useradmin.console.config',
			'useradmin.console.prepare',
			'useradmin.console.migrate',
			'useradmin.console.useradd',
			'useradmin.console.userfind',
			'useradmin.console.userupdate'
		);
	}

	public function bootValidator()
	{
		$this->app['useradmin.validation.presence'] = $this->app->share(function($app)
		{
			$presence = new DatabasePresenceVerifier($app['db']);

			$presence->setConnection(connection_name());

			return $presence;
		});

		$this->app['useradmin.validator'] = $this->app->share(function($app)
		{
			$validator = new Factory($app['translator'], $app);

			if (isset($app['useradmin.validation.presence']))
			{
				$validator->setPresenceVerifier($app['useradmin.validation.presence']);
			}

			// we need to extend our custom validator with the functions specified in the packages we depend on

			// from package hampel/validate-laravel

			$validatorNamespace = 'Hampel\Validate\Laravel\\';

			$translation = $app['translator']->get('validate-laravel::validation');

			$validator->extend('unique_or_zero', $validatorNamespace . 'Validator@validateUniqueOrZero', $translation['unique_or_zero']);
			$validator->extend('exists_or_zero', $validatorNamespace . 'Validator@validateExistsOrZero', $translation['exists_or_zero']);
			$validator->extend('bool', $validatorNamespace . 'Validator@validateBool', $translation['bool']);
			$validator->extend('ipv4_public', $validatorNamespace . 'Validator@validateIpv4Public', $translation['ipv4_public']);
			$validator->extend('ipv6_public', $validatorNamespace . 'Validator@validateIpv6Public', $translation['ipv6_public']);
			$validator->extend('ip_public', $validatorNamespace . 'Validator@validateIpPublic', $translation['ip_public']);
			$validator->extend('domain', $validatorNamespace . 'Validator@validateDomain', $translation['domain']);
			$validator->extend('domain_in', $validatorNamespace . 'Validator@validateDomainIn', $translation['domain_in']);
			$validator->extend('tld', $validatorNamespace . 'Validator@validateTLD', $translation['tld']);
			$validator->extend('tld_in', $validatorNamespace . 'Validator@validateTLDIn', $translation['tld_in']);
			$validator->extend('uploaded_file', $validatorNamespace . 'Validator@validateUploadedFile', $translation['uploaded_file']);

			// from package hampel/validate-laravel-auth

			$translation = $app['translator']->get('validate-laravel-auth::validation');

			$validator->extend('auth', 'Hampel\Validate\LaravelAuth\Validator@validateAuth', $translation['auth']);

			return $validator;
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}