<?php namespace Hampel\Admin\Users\Validators;
/**
 * 
 */

use Illuminate\Validation\Factory;

class UserValidator
{
	protected $validator;

	public function __construct(Factory $validator)
	{
		$this->validator = $validator;
	}

	protected function validate($data, $rules, array $translation = array())
	{
		return $this->validator->make($data, $rules, $translation);
	}

	public function validateLogin($username, $password)
	{
		$user_data = array(
			'username' => $username,
			'password' => $password
		);

		$credential_field = credential_field($username);

		$rules = array(
			'username' => array('required', 'exists:' . users_table() . ',' . $credential_field),
			'password' => array('required', "auth:{$credential_field},{$username}")
		);

		// Validate the inputs.
		$validator = $this->validate($user_data, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validateEmail($email)
	{
		$userdata = array(
			'email' => $email,
		);

		// Declare the rules for the form validation.
		$rules = array(
			'email'  => array('required', 'email', 'exists:users'),
		);

		// Validate the inputs.
		$validator = $this->validate($userdata, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validatePasswordReset($email, $password, $password_confirmation)
	{
		// Get all the inputs
		$resetdata = array(
			'email' => $email,
			'password' => $password,
			'password_confirmation' => $password_confirmation,
		);

		// Declare the rules for the form validation.
		$rules = array(
			'email'  => array('required', 'email', 'exists:users', 'exists:password_reminders'),
			'password' => array('required', 'confirmed', 'min:8'),
		);

		// Validate the inputs.
		$validator = $this->validate($resetdata, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validatePasswordChange($password, $new_password, $new_password_confirmation)
	{
		// Get all the inputs
		$resetdata = array(
			'password' => $password,
			'new_password' => $new_password,
			'new_password_confirmation' => $new_password_confirmation,
		);

		// Declare the rules for the form validation.
		$rules = array(
			'password'  => array('required', 'auth:' . username_field() . ',' . current_username()),
			'new_password' => array('required', 'confirmed', 'min:8'),
		);

		// Validate the inputs.
		$validator = $this->validate($resetdata, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validateEmailChange($email, $password)
	{
		// Get all the inputs
		$email_data = array(
			'email' => $email,
			'password' => $password,
		);

		// Declare the rules for the form validation.
		$rules = array(
			'email' => array('required', 'email'),
			'password' => array('required', 'auth:' . username_field() . ',' . current_username()),
		);

		// Validate the inputs.
		$validator = $this->validate($email_data, $rules);

		$validator->passes();

		return $validator->errors();
	}
}

?>
