<?php namespace Hampel\Admin\Users\Controllers;
/**
 * 
 */

use View, Input, Redirect;
use Hampel\Admin\Users\Repositories\UserRepositoryInterface;

class ProfileController extends BaseController {

	protected $users;

	public function __construct(UserRepositoryInterface $users)
	{
		parent::__construct();

		$this->users = $users;

		$this->beforeFilter('useradmin.auth');
	}

	/**
	 * Show Profile
	 *
	 * @return mixed
	 */
	public function getProfile()
	{
		return View::make('useradmin::profile.show');
	}


	/**
	 * Show Password Change Form
	 *
	 * @return mixed
	 */
	public function getChangePassword()
	{
		return View::make('useradmin::profile.changepassword');
	}

	/**
	 * Process Password Change
	 *
	 * @return mixed
	 */
	public function postChangePassword()
	{
		$passwordChange = $this->users->passwordChange(Input::get('password'), Input::get('new_password'), Input::get('new_password_confirmation'));

		if ($passwordChange === true)
		{
			return $this->redirectProfile('useradmin::profile.password.success');
		}
		elseif ($passwordChange === false)
		{
			return Redirect::route('useradmin.profile.change-password')->with('error', true)->with('reason', 'useradmin::profile.password.failed');
		}
		else
		{
			return Redirect::route('useradmin.profile.change-password')->withErrors($passwordChange);
		}
	}

	/**
	 * Show Email Change Form
	 *
	 * @return mixed
	 */
	public function getChangeEmail()
	{
		return View::make('useradmin::profile.changeemail');
	}

	/**
	 * Process Email Change
	 *
	 * @return mixed
	 */
	public function postChangeEmail()
	{
		$emailChange = $this->users->emailChange(Input::get('email'), Input::get('password'));

		if ($emailChange === true)
		{
			return $this->redirectProfile('useradmin::profile.email.success');
		}
		elseif ($emailChange === false)
		{
			return Redirect::route('useradmin.profile.change-email')->with('error', true)->with('reason', 'useradmin::profile.email.failed');
		}
		else
		{
			return Redirect::route('useradmin.profile.change-email')->withErrors($emailChange);
		}
	}

	protected function redirectProfile($reason)
	{
		// Redirect to homepage
		return Redirect::route('useradmin.profile.show')->with('success', true)->with('reason', $reason);
	}
}


?>
