<?php namespace Hampel\Admin\Users\Controllers;

use Controller, App;
use Illuminate\Auth\Guard;

class BaseController extends Controller {

	protected $auth;

	public function __construct()
	{
		$this->auth = App::make('auth')->driver();
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}