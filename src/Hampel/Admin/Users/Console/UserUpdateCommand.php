<?php namespace Hampel\Admin\Users\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Hampel\Admin\Users\Repositories\UserRepositoryInterface;

class UserUpdateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'user:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = "Update a user. Use user:find command to find the user's ID";

	protected $users;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepositoryInterface $users)
	{
		$this->users = $users;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$user = $this->users->findById($this->argument('id'));

		if (is_null($user))
		{
			$this->error('No user found for id: ' . $this->option('id'));
			return;
		}

		$updateMade = false;

		if (!empty($this->option('username')))
		{
			$username_field = username_field();
			$user->$username_field = $this->option('username');
			$updateMade = true;
		}

		if (!empty($this->option('email')))
		{
			$email_field = email_field();
			$user->$email_field = $this->option('email');
			$updateMade = true;
		}

		if (!empty($this->option('password')))
		{
			$password_field = password_field();
			$user->$password_field = $this->option('password');
			$updateMade = true;
		}

		if ($updateMade)
		{
			if ($user->save())
			{
				$this->info('User successfully updated');
			}
			else
			{
				$this->error('Error updating user');
			}
		}
		else
		{
			$this->error('No update options specified');
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('id', InputArgument::REQUIRED, 'ID of user to update.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('username', 'u', InputOption::VALUE_OPTIONAL, 'Update username.', null),
			array('email', 'e', InputOption::VALUE_OPTIONAL, 'Update email address.', null),
			array('password', 'p', InputOption::VALUE_OPTIONAL, 'Update password.', null),
		);
	}

}