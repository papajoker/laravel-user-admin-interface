<?php namespace Hampel\Admin\Users\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Hampel\Admin\Users\Repositories\UserRepositoryInterface;

class UserFindCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'user:find';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Find a user by attribute';

	protected $users;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepositoryInterface $users)
	{
		$this->users = $users;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		if (!empty($this->option('username')))
		{
			$user = $this->users->findByUsername($this->option('username'));

			if (is_null($user))
			{
				$this->error('No user found for username: ' . $this->option('username'));
				return;
			}
		}
		elseif (!empty($this->option('email')))
		{
			$user = $this->users->findByEmail($this->option('email'));

			if (is_null($user))
			{
				$this->error('No user found for email: ' . $this->option('email'));
				return;
			}
		}
		elseif (!empty($this->option('id')))
		{
			$user = $this->users->findById($this->option('id'));

			if (is_null($user))
			{
				$this->error('No user found for id: ' . $this->option('id'));
				return;
			}
		}
		else
		{
			$this->error('No options specified - please specify at least one option');
			return;
		}

		$username_field = username_field();
		$email_field = email_field();

		$this->info('Username: ' . $user->$username_field);
		$this->info('Email: ' . $user->$email_field);
		$this->info('User ID: ' . $user->id);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('username', 'u', InputOption::VALUE_OPTIONAL, 'Find user by username.', null),
			array('email', 'e', InputOption::VALUE_OPTIONAL, 'Find user by email address.', null),
			array('id', 'i', InputOption::VALUE_OPTIONAL, 'Find user by ID.', null),
		);
	}

}