<?php namespace Hampel\Admin\Users\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Hampel\Admin\Users\Repositories\UserRepositoryInterface;

class UserAddCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'user:add';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Add a new user';

	protected $users;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepositoryInterface $users)
	{
		$this->users = $users;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->users->create($this->argument('username'), $this->argument('email'), $this->argument('password'));

		$this->info('Created user ' . $this->argument('username'));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('username', InputArgument::REQUIRED, 'Username'),
			array('email', InputArgument::REQUIRED, 'Email address'),
			array('password', InputArgument::REQUIRED, 'Password'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}