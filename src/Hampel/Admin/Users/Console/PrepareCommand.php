<?php namespace Hampel\Admin\Users\Console;

use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PrepareCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'useradmin:prepare';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Prepare database tables for installation';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->call('migrate:install', array('--database' => Config::get('useradmin::database.connection')));
	}

}