<?php namespace Hampel\Admin\Users\Repositories;
/**
 * 
 */

use Auth, Password;
use Hampel\Admin\Users\Models\User;
use Hampel\Admin\Users\Validators\UserValidator;

class EloquentUserRepository implements UserRepositoryInterface
{

	/**
	 * @var \Hampel\Admin\Users\Validators\UserValidator
	 */
	protected $userValidator;

	public function __construct(UserValidator $userValidator)
	{
		$this->userValidator = $userValidator;
	}

	public function current()
	{
		return Auth::user();
	}

	public function create($username, $email, $password)
	{
		$user_model = user_model();

		return $user_model::create(compact('username', 'email', 'password'));
	}

	public function findByUsername($username)
	{
		$username_field = username_field();

		return User::where($username_field, "=", $username)->first();
	}

	public function findByEmail($email)
	{
		$email_field = email_field();

		return User::where($email_field, "=", $email)->first();
	}

	public function findById($id)
	{
		return User::find($id)->first();
	}

	public function login($username, $password, $remember = false)
	{
		$errors = $this->userValidator->validateLogin($username, $password);

		if (!$errors->isEmpty()) return $errors;

		return $this->authenticateUsername($username, $password, $remember);
	}

	protected function authenticateUsername($username, $password, $remember = false)
	{
		$credentials = array(
			credential_field($username) => $username,
			password_field() => $password
		);

		return Auth::attempt($credentials, $remember);
	}

	protected function authenticateEmail($email, $password, $remember = false)
	{
		$credentials = array(
			email_field() => $email,
			password_field() => $password
		);

		return Auth::attempt($credentials, $remember);
	}

	public function lostPassword($email)
	{
		$errors = $this->userValidator->validateEmail($email);

		if (!$errors->isEmpty()) return $errors;

		return true;
	}

	public function sendReminder($email)
	{
		$credentials = array(
			email_field() => $email,
		);

		return Password::remind($credentials);
	}

	public function passwordReset($email, $password, $password_confirmation)
	{
		$errors = $this->userValidator->validatePasswordReset($email, $password, $password_confirmation);

		if (!$errors->isEmpty()) return $errors;

		return $this->resetPassword($email, $password);
	}

	protected function resetPassword($email, $password)
	{
		$credentials = array(
			email_field() => $email
		);

		return Password::reset($credentials, function($user, $password)
		{
			$password_field = password_field();

			$user->$password_field = $password;
			return $user->save();
		});
	}

	public function passwordChange($password, $new_password, $new_password_confirmation)
	{
		$errors = $this->userValidator->validatePasswordChange($password, $new_password, $new_password_confirmation);

		if (!$errors->isEmpty()) return $errors;

		return $this->changePassword($new_password);
	}

	protected function changePassword($new_password)
	{
		$password_field = password_field();

		$user = $this->current();
		$user->$password_field = $new_password;
		return $user->save();
	}

	public function emailChange($email, $password)
	{
		$errors = $this->userValidator->validateEmailChange($email, $password);

		if (!$errors->isEmpty()) return $errors;

		return $this->changeEmail($email);
	}

	protected function changeEmail($new_email)
	{
		$email_field = email_field();

		$user = $this->current();
		$user->$email_field = $new_email;
		return $user->save();
	}
}

?>
