<?php namespace Hampel\Admin\Users\Repositories;
/**
 * 
 */

interface UserRepositoryInterface
{
	/**
	 * Return the currently logged in user
	 *
	 * @return User
	 */
	public function current();

	public function findByUsername($username);

	public function findByEmail($email);

	public function findById($id);

	public function create($username, $email, $password);

	public function login($username, $password, $remember = false);

	public function lostPassword($email);

	public function sendReminder($email);

	public function passwordReset($email, $password, $password_confirmation);

	public function passwordChange($password, $new_password, $new_password_confirmation);

	public function emailChange($email, $password);
}

?>
