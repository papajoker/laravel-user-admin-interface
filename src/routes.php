<?php

/*
|--------------------------------------------------------------------------
| Package Routes
|--------------------------------------------------------------------------
|
| Routes for our package
|
*/

$controllerNameSpace = 'Hampel\Admin\Users\Controllers\\';

Route::group(Config::get('useradmin::routes.admin_group_rules'), function() use ($controllerNameSpace)
{
	/**
	 * Login / Logout forms
	 */
	Route::get('login', array('uses' => $controllerNameSpace . 'AuthController@getLogin', 'as' => 'useradmin.auth.login'));
	Route::post('login', array('uses' => $controllerNameSpace . 'AuthController@postLogin'));
	Route::get('logout', array('uses' => $controllerNameSpace . 'AuthController@getLogout', 'as' => 'useradmin.auth.logout'));

	/**
	 * Lost Password Form
	 */
	Route::get('password/lost', array('uses' => $controllerNameSpace . 'AuthController@getLostPassword', 'as' => 'useradmin.auth.lost-password'));
	Route::post('password/lost', array('uses' => $controllerNameSpace . 'AuthController@postLostPassword'));

	/**
	 * Password Reset Form
	 */
	Route::get('password/reset/{token}', array('uses' => $controllerNameSpace . 'AuthController@getResetPassword', 'as' => 'useradmin.auth.reset-password'));
	Route::post('password/reset/{token}', array('uses' => $controllerNameSpace . 'AuthController@postResetPassword'));
});

Route::group(Config::get('useradmin::routes.profile_group_rules'), function() use ($controllerNameSpace)
{
	Route::get('profile', array('uses' => $controllerNameSpace . 'ProfileController@getProfile', 'as' => 'useradmin.profile.show'));

	/**
	 * Change Password form
	 */
	Route::get('password/change', array('uses' => $controllerNameSpace . 'ProfileController@getChangePassword', 'as' => 'useradmin.profile.change-password'));
	Route::post('password/change', array('uses' => $controllerNameSpace . 'ProfileController@postChangePassword'));

	/**
	 * Change Email form
	 */
	Route::get('email/change', array('uses' => $controllerNameSpace . 'ProfileController@getChangeEmail', 'as' => 'useradmin.profile.change-email'));
	Route::post('email/change', array('uses' => $controllerNameSpace . 'ProfileController@postChangeEmail'));
});

?>
