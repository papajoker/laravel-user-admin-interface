<?php
/**
 * 
 */

function credential_field($username)
{
	if (is_email($username))
	{
		return email_field();
	}
	else
	{
		return username_field();
	}
}

function is_email($value)
{
	return App::make('validate-laravel.validator')->isEmail($value);
}

function username_field()
{
	return Config::get('useradmin::database.username_field');
}

function email_field()
{
	return Config::get('useradmin::database.email_field');
}

function password_field()
{
	return Config::get('useradmin::database.password_field');
}

function connection_name()
{
	return Config::get('useradmin::database.connection');
}

function base_layout()
{
	return Config::get('useradmin::views.base_layout');
}

function users_table()
{
	return Config::get('auth.table');
}

function user_model()
{
	return Config::get('auth.model');
}

function home_route_name()
{
	return Config::get('useradmin::routes.home');
}

?>
