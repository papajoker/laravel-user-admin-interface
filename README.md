User Admin Interface for Laravel
================================

This package provides basic user administration capabilities for Laravel 4 applications.

This includes login/logout, password reset, password change and email change functionality.

Admin tools for creating and updating users from the artisan command line are also included.

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing the User Admin Interface is through [Composer](http://getcomposer.org):

Require the package via Composer in your `composer.json`

	:::json
    {
        "require": {
            "hampel/useradmin": "1.0.*"
        }
    }

Run Composer to update the new requirement.

	:::bash
    $ composer update

The package is built to work with the Laravel 4 Framework.

Open your Laravel config file `config/app.php` and add the following service providers in the `$providers` array, if they don't already exist:

	:::php
    "providers" => array(

        ...

		'Hampel\Validate\Laravel\ValidateServiceProvider',
		'Hampel\Validate\LaravelAuth\ValidateServiceProvider',
		'Hampel\Admin\Users\UserAdminServiceProvider',

    ),

Configure `app/config/app.php`, including specifying a random long string for the encryption key.

Configure `app/config/auth.php`,

1. change __model__ to `'Hampel\Admin\Users\Models\User'`, or specify your own model, but if you do, you should have your model extend
 'Hampel\Admin\Users\Models\User'
2. change __reminder.email__ to `'useradmin::emails.auth.reminder'`, or specify your own email template for reminder emails

Note that you can change the __auth.table__ and __auth.reminder.table__ tables, but if you do so, you will need to create your own
 migrations to set up the appropriate fields for each table

Configure `app/config/database.php` - set up your database connections. The User Admin package supports using separate database connections
for the user management tables and the core application itself - for example, if adding an administration layer on top of an existing
 application, you might not want to store the user tables in the application database. Similarly, even if you are happy to store the user
 data in the same database as your application, you might want to specify a prefix name to avoid table name clashes.

Either way, you can set up two different database connections and configure the User Admin options (described later) to use a separate
 connection - your main application will use the default database connection.

Publish the User Admin configuration using the command:

	:::bash
	$ php artisan useradmin:config

Configure `app/config/packages/hampel/useradmin/database.php`,

1. set __connection__ to the name of the database connection you previously set up for the user admin database. If you set this to the value
`'default'`, it will use the same database connection as used by your main application.
2. if using a custom user table, you may specify field names for the key username, email and password fields

Configure `app/config/packages/hampel/useradmin/routes.php` if you would like to change the default route options.

Configure `app/config/packages/hampel/useradmin/views.php` to set the name of your base layout for views. Read the instructions in the
 config file for details of what is required in your layout.

If you are using your own custom database migrations, you can skip the following two steps.

Prepare the database for migrations by using the command:

	:::bash
	$ php artisan useradmin:prepare

Run the migrations using the command:

	:::bash
	$ php artisan useradmin:migrate

Create an admin user using the artisan command:

	:::bash
	$ php artisan user:add <username> <email> <password>

Visit the URL `/profile` (relative to your application URL), you will be redirected to the login page, where you may log in using
 the username and password you just specified. The logged in user can change their password or email addresses from the profile page.

Usage
-----

... TODO !!