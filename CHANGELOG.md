CHANGELOG
=========

1.0.5 (2013-10-21)
------------------

* added `class="error"` to error messages for Foundation
* moved lang helper functions to separate file
* implemented passing of translation array to validator via Factory::extend method
* minimum version of illuminate/validation set to 4.0.9 to use additional parameter on Factory::extend

1.0.4 (2013-10-20)
------------------

* separated user validation functions into separate class, injected into user repository in service provider

1.0.3 (2013-10-17)
------------------

* added custom validator to package so we can use a separate database connection for presence verification
* fixed bug in EloquentUserRepository where emails were not accepted for login

1.0.2 (2013-10-17)
------------------

* removed database.users_table config option in favour of using core auth.table value instead
* changed migration to use hard-coded table name, implementer will need to provide their own migrations if want to use a different name
* cleaned up Service Provider code
* updated User model to use configuration options for table and fillable
* update README with installation information

1.0.1 (2013-10-16)
------------------

* added hampel/validate-laravel package to requirements in composer.json

1.0.0 (2013-10-16)
------------------

* initial release
